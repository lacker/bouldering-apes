<div align="center">

<picture>
<img src="https://gitlab.com/lacker/bouldering-apes/-/raw/master/logo.svg?ref_type=heads" width=600> 
</picture>

</div>

# Before starting

Make sure python, Pillow and opencv are installed.

# How to use it to create a boulder

Today, a boulder is a sequence of holds (alternating right hand and left hand)
with a free choice of foot holds. To be able to create your own boulder.

1. Take a picture of the wall you wan to use, resize it to be 1000px high. Save
   it as <wall.png>.
2. Produce a saturated version and a grayed version of this wall <wall_bw.png>
   and <wall_sat.png>
3. Produce a mask <mask.png> for the holds. It is a black picture with holds
   delimited with white (the outer contours will be used for detection).
4. Place all these files into <./walls/name_of_your_wall/>.
5. Change WALL_NAME in the <constants.py> file and run the app.

When running the app with a new hold, segmentation is performed and it can take
a bit of time (30s max); later, loading the wall will be much faster (1s). If
you update the mask.png file, make sure to delete the
<./walls/name_of_your_wall/holds> folder to force segmentation.

Then, left_click on a hold to add it to the boulder. Right_click on the last
added hold to remove it from the boulder.


