from tkinter import *
from PIL import Image, ImageTk
from constants import *
import json

class NoHoldAtPoint(Exception):
    "Raised when there is no hold under the cursor"
    pass

def display_main_screen(wall_path=WALL_PATH):

    # Create the layout
    main_screen_layout = create_main_screen_layout(wall_path)

    # Fill the layout
    canva = display_canva(main_screen_layout["canva"], wall_path)
    menu  = display_menu()

    main_screen_layout["root"].mainloop()


def create_main_screen_layout(wall_path=WALL_PATH):
    '''
    Create the frame, and return the dictionnary of the structures
    '''

    # Recover wall size
    with Image.open(f"{wall_path}/wall_bw.png") as wall_bw:
        (wall_w, wall_h) = wall_bw.size

    # Create root
    root = Tk()
    root.title("Bouldering apes")
    root.geometry(f"{wall_w + MENU_WIDTH}x{WINDOW_HEIGHT}")
    root.attributes('-type', 'dialog') # Ensure float format for tilling WM

    # Create frames
    menu_frame  = Frame(root, width=MENU_WIDTH, height=WINDOW_HEIGHT, bg='green')
    menu_frame.grid(row=0, column=0)
    image_frame = Frame(root, width=wall_w, height=WINDOW_HEIGHT, bg='red')
    image_frame.grid(row=0, column=1)

    # Create canva
    cnv = Canvas(image_frame, width=wall_w, height=1000)
    cnv.grid(row=0, column=0)

    main_screen = {
        "root"        : root,
        "menu_frame"  : menu_frame,
        "image_frame" : image_frame,
        "canva"       : cnv }

    return main_screen


def display_canva(cnv, wall_path):

    global holds
    global bldr
    global holds_proc_ord
    global bckgrnd
    global bckgrnd_id
    global line_sublayer
    global holds_id_to_name

    holds = [] # List of dictionnary (sat_id, bw_id, text, coords)
    holds_id_to_name = {}
    bldr = {
        "bldr_holds"     : [], # hold_name
        "bldr_lines_ids" : [], #
    }

    # Load background, hold positions, hold images
    bckgrnd   = PhotoImage(file=f"{WALL_PATH}/wall_bw.png")
    holds_pos = parse_holds_pos(wall_path)
    for i in range(len(holds_pos)):
        hold_dic = {
            "bw_img"  : PhotoImage(file=f"{wall_path}/holds/hold_{i}_bw.png"),
            "sat_img" : PhotoImage(file=f"{wall_path}/holds/hold_{i}_sat.png"),
            "coord_NW" : holds_pos[i]["top"],
            "coord_C"  : holds_pos[i]["center"],
            "coord_text" : (holds_pos[i]["center"][0], holds_pos[i]["top"][1]),
            "text_id" : 0,
            "bw_id"   : 0,
            "sat_id"  : 0,
            "steps"   : []
        }
        holds.append(hold_dic)

    # Compute the hold processing area: hold names from biggest to smallest
    holds_areas = [(i, holds[i]["bw_img"].width() * holds[i]["bw_img"].height()) \
                   for i in range(len(holds))]
    holds_proc_ord = [i for (i, a) in sorted(holds_areas, key= lambda t: -t[1])]

    # Create canva objects, in the right order. Update structures if needed.
    # 1- Background
    # 2- Gray holds
    # 3- Lines (not created here)
    # 4- Sat holds (hidden here)
    # 5- Text
    bckgrnd_id = cnv.create_image(0, 0, image=bckgrnd, anchor=NW)
    for i in holds_proc_ord:
        h = cnv.create_image(holds[i]["coord_NW"][0], holds[i]["coord_NW"][1], \
                            image=holds[i]["bw_img"], anchor=NW)
        holds[i]["bw_id"] = h
        holds_id_to_name[h] = i
        cnv.tag_bind(h, '<Button-1>', lambda e: left_click_on_hold(cnv, e))
        cnv.tag_bind(h, '<Button-3>', lambda e: right_click_on_hold(cnv, e))
    for i in holds_proc_ord:
        h = cnv.create_image(holds[i]["coord_NW"][0], holds[i]["coord_NW"][1], \
                             image=holds[i]["sat_img"], anchor=NW,
                             state="hidden")
        holds[i]["sat_id"] = h
        holds_id_to_name[h] = i
        cnv.tag_bind(h, '<Button-1>', lambda e: left_click_on_hold(cnv, e))
        cnv.tag_bind(h, '<Button-3>', lambda e: right_click_on_hold(cnv, e))
    for i in holds_proc_ord:
        t = cnv.create_text(holds[i]["coord_text"], anchor=S, text ="", \
                            fill ="cyan", font="Arial 12 bold")
        holds[i]["text_id"] = t

    # Id of the highest gray hold, for positioning lines
    line_sublayer = holds[holds_proc_ord[-1]]["bw_id"]

def hold_name_at_point(cnv, event):
    '''
    Returns the hold_name at point, or raise an exception
    '''

    global bckgrnd_id
    global line_sublayer

    overlap = cnv.find_overlapping(event.x, event.y, event.x + 1, event.y + 1)
    gray_overlap = list(filter(lambda x: (x != bckgrnd_id) and (x <= line_sublayer), \
                               overlap))
    for hold_id in gray_overlap:
        hold_name = holds_id_to_name[hold_id]
        if not is_alpha_at_point(hold_name, event.x, event.y):
            return hold_name
    raise NoHoldAtPoint

def is_alpha_at_point(hold_name, x, y):
    (x_anchor, y_anchor) =  holds[hold_name]["coord_NW"]
    return holds[hold_name]["bw_img"].transparency_get(x - x_anchor, \
                                                       y - y_anchor)


def left_click_on_hold(cnv, event, mode="default"):
    if mode == "default":
        try:
            hold_name = hold_name_at_point(cnv, event)

            # Add to boulder
            bldr["bldr_holds"].append(hold_name)
            holds[hold_name]["steps"].append(len(bldr["bldr_holds"]))

            # Update text
            t = '/'.join(list(map(str, holds[hold_name]["steps"])))
            cnv.itemconfigure(holds[hold_name]["text_id"], text=t)

            # Ensure the hold is colored
            cnv.itemconfigure(holds[hold_name]["sat_id"], state="normal")

            # Plot line
            if len(bldr["bldr_holds"]) >= 2:
                (s, t) = (bldr["bldr_holds"][-2], bldr["bldr_holds"][-1])
                (s_x, s_y) = holds[s]["coord_C"]
                (t_x, t_y) = holds[t]["coord_C"]
                l =  cnv.create_line(s_x, s_y, t_x, t_y, fill='white', width=4)
                bldr["bldr_lines_ids"].append(l)

                # Put the line under the holds
                # cnv.tag_lower(l)
                cnv.tag_raise(l, line_sublayer)

        except NoHoldAtPoint:
            pass



def right_click_on_hold(cnv, event, mode="default"):
    if mode == "default":
        try:
            hold_name = hold_name_at_point(cnv, event)

            # Check that we try to remove ony the last hold
            if bldr["bldr_holds"] != [] and bldr["bldr_holds"][-1] == hold_name:

                # Remove from boulder
                bldr["bldr_holds"].pop()
                holds[hold_name]["steps"].pop()

                # Update text
                t = '/'.join(list(map(str, holds[hold_name]["steps"])))
                cnv.itemconfigure(holds[hold_name]["text_id"], text=t)

                # Check the hold color
                if hold_name not in bldr["bldr_holds"]:
                    cnv.itemconfigure(holds[hold_name]["sat_id"], state="hidden")

                if bldr["bldr_lines_ids"] != []:
                    l = bldr["bldr_lines_ids"].pop()
                    cnv.delete(l)

        except NoHoldAtPoint:
            pass

def parse_holds_pos(wall_path):
    with open(f"{WALL_PATH}/holds/holds_pos.txt",'r') as txt:
        holds_pos = txt.read().splitlines()
        return list(map(eval, holds_pos))

def display_menu():
    pass
