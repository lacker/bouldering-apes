import os
import cv2
from constants import *
from rio_extract import *

def ensure_user_provided_files(wall_path=WALL_PATH):
    '''
    Ensure that ./{wall_path}/wall.png and ./{wall_path}/mask.png exist
    '''

    if (not os.path.exists(f"{wall_path}/wall.png")):
        print(f"{wall_path}/wall.png is missing!") # TODO: switch to error
    if (not os.path.exists(f"{wall_path}/mask.png")):
        print(f"{wall_path}/mask.png is missing!") # TODO: switch to error

def ensure_holds_folder_exists(wall_path=WALL_PATH):
    '''
    Create the ./{wall_path}/holds folder if it doesn't exist
    '''
    if (not os.path.exists(f"{wall_path}/holds/")):
        os.mkdir(f"{wall_path}/holds/")

def ensure_different_wall_versions_exist(wall_path=WALL_PATH):
    '''
    Create the ./{wall_path}/wall_bw.png and .../wall_sat.png if they don't
    exist
    '''
    if (not os.path.exists(f"{wall_path}/wall_sat.png")):
        pass # TODO
    if (not os.path.exists(f"{wall_path}/wall_bw.png")):
        pass # TODO

def ensure_holds_are_extracted(wall_path=WALL_PATH):
    '''
    Generate ./wall/WALLNAME/holds/* if it doesn't exist. The later containes
    - The croped picture of every hold in two versions: colored and grayed
    - The list of the positions of those holds, within <holds_pos.txt>
    '''
    if (not os.path.exists(f"{wall_path}/holds/holds_pos.txt")):
        cts = compute_outer_contours(wall_path)
        extract_holds(cts, wall_path, "_sat")
        extract_holds(cts, wall_path, "_bw")
