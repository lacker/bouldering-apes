import os
import cv2
from tkinter import *
from PIL import Image, ImageTk
from preprocessing import *
from constants import *
from main_screen import *

# .
# ├── main.py
# ├── rio_extract.py
# └── walls
#     └── dev_wall
#         ├── mask.png
#         ├── wall.png
#         ├── wall_bw.png
#         ├── wall_sat.png
#         ├── wall_mask.png
#         ├── holds
#         │   ├── hold_0_bw.png
#         │   ├── hold_0_sat.png
#         │   ├── hold_1_bw.png
#         │   ├── hold_1_sat.png
#         │   ├── hold_2_bw.png
#         │   ├── hold_2_sat.png
#         │   ├── ...
#         │   ├── hold_N_bw.png
#         │   └── hold_N_sat.png
#         ·


# Preprocessing
ensure_user_provided_files()
ensure_holds_folder_exists()
ensure_different_wall_versions_exist()
ensure_holds_are_extracted()

# Display main screen
display_main_screen()

# Framework:
# OFFLINE
# (1) Identifier les 4 sommets du pan, et en faire un rectangle d'aspect-ratio donné
# (2) Colorier les prises en noir, en laissant des espaces vides
# ONLINE
# (3) Segmenter l'image: générer des images des prises (fond alpha) et de leurs coordonnées
# (4) Afficher un fond + ces images dans un canva, les rendre visible au clic (afficher les images de la plus petite à la plus grande)
