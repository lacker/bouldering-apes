import cv2
import numpy as np
from constants import *

def compute_outer_contours(wall_path):
    '''
    Given a wall_path, return the list of the outer contours of holds by
    exploiting the mask.
    '''

    # Load the mask
    mask = cv2.imread(f"{wall_path}/mask.png")

    # Ensure the mask is binary
    mask_gray   = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    _, mask_bin = cv2.threshold(mask_gray, 150, 255, cv2.THRESH_BINARY)

    # Detect the contours and compute a 2-hieratchy with RETR_CCOMP
    cts, hierarchy = cv2.findContours(image=mask_bin, mode=cv2.RETR_CCOMP, \
                                           method=cv2.CHAIN_APPROX_NONE)

    # Filter the outer contours (without parents in the CCOMP hierarchy) and
    # return them
    outer_cts = []
    for i, contour in enumerate(cts):
        if hierarchy[0][i][3] == -1:
            outer_cts.append(contour)
    return outer_cts


def extract_hold(contour, index, wall_path, suffix):
    '''
    Given a contour, an index, a wall_path and a suffix (_bw or _sat), extract
    the hold as a small image with transparent background, and return its
    position (top-left) in the original image.
    '''

    # Load the image, add an alpha channel and recover its dimensions
    wall = cv2.cvtColor(cv2.imread(f"{WALL_PATH}/wall{suffix}.png"),
                        cv2.COLOR_RGB2RGBA)
    (h, w, _) = wall.shape

    # Build mask from contour
    mask = np.zeros((h, w), dtype="uint8")
    cv2.fillPoly(mask, pts=[contour], color=(255,255,255))

    # Determine the bounding box of the mask
    (y, x) = np.where(mask == 255)
    (top_y, top_x) = (np.min(y), np.min(x))
    (bottom_y, bottom_x) = (np.max(y), np.max(x))
    (mid_y, mid_x) = (int((top_y + bottom_y)/2), int((top_x + bottom_x)/2))

    # Extract hold
    hold = cv2.bitwise_and(wall, wall, mask=mask)
    hold = hold[top_y:bottom_y+1, top_x:bottom_x+1]

    cv2.imwrite(f"{wall_path}/holds/hold_{index}{suffix}.png", hold)

    return (top_x, top_y, mid_x, mid_y)


def extract_holds(contours, wall_path, suffix):
    '''
    Given a list of contours, a wall_path and a suffix (_bw or _sat), generate
    the holds pictures and the .txt file containing their position on the wall
    While this function is typically called twice (once per suffix), the
    coordinate register is only written once.
    '''

    if suffix == "_sat":
        f = open(f"{wall_path}/holds/holds_pos.txt", "w")
        for i in range(len(contours)):
            (top_x, top_y, mid_x, mid_y) = extract_hold(contours[i], i, \
                                                        wall_path, suffix)
            line = f'{{"top" : ({top_x}, {top_y}), "center" : ({mid_x}, {mid_y})}}\n'
            f.write(line)
        f.close()
    else:
        for i in range(len(contours)):
            _ = extract_hold(contours[i], i, wall_path, suffix)
